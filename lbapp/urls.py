from django.urls import path
from . import views

urlpatterns = [
    path('',views.index,name='home'),

    path('upage/',views.upage,name='upage'),
    path('upage/all/', views.ulist , name = 'ulist'),
    path('udel/<int:id>',views.udel, name = 'udel'),
    path('uedit/<int:id>',views.uedit, name = 'uedit'),
   # path('upage/update/<int:id>',views.upage,),


    path('ppage/',views.ppage,name='ppage'),
    path('ppage/all/', views.plist , name = 'plist'),
    path('pdel/<int:id>',views.pdel, name = 'pdel'),
    path('pedit/<int:id>',views.pedit, name = 'pedit'),

   

    path('opage/',views.opage,name='opage'),
    path('opage/all/', views.olist , name = 'olist'),
    path('odel/<int:id>',views.odel, name = 'odel'),
    path('oedit/<int:id>',views.oedit, name = 'oedit'),

]
