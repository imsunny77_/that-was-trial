from django.apps import AppConfig


class LbappConfig(AppConfig):
    name = 'lbapp'
