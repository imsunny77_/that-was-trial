from django.shortcuts import render, redirect
from .forms import *

# Create your views here.

def index(request):
    return render(request,'index.html')

def upage(request):
    if request.method == 'POST':
        form1 = AddUser(request.POST)
        if form1.is_valid():
            form1.save()
            return redirect(index)
    form1 = AddUser()
    return render(request,'upage.html',{'form':form1})

def ppage(request):
    if request.method == 'POST':
        form2 = AddProduct(request.POST)
        if form2.is_valid():
            form2.save()
            return redirect(index)
    form2= AddProduct
    return render(request,'ppage.html',{'form':form2})
        
def opage(request):
    
    if request.method == 'POST':
        form3 = AddOrder(request.POST)
        if form3.is_valid():
            form3.save()
            return redirect(index)
    form3= AddOrder
    return render(request,'opage.html',{'form':form3})

def ulist(request):
    ulist = User.objects.all
   # udict = {'upage':ulist}
    return render(request, 'ulist.html', {'upage':ulist})
   # return render {request, 'templates/ulist.html', context = udict

def plist(request):
    plist = Product.objects.all
    return render (request , 'plist.html',{'ppage':plist})

def olist(request):
    olist = Order.objects.all
    return render (request , 'olist.html',{'opage':olist})

def udel(request, id):
    udel = User.objects.get(id = id)
    udel.delete()
    return ulist(request)
        
def pdel(request, id):
    pdel = Product.objects.get(id = id)
    pdel.delete()
    return plist(request)

def odel(request, id):
    odel = Order.objects.get(id = id)
    odel.delete()
    return olist(request)

def uedit(request, id):
    uedit = User.objects.get(id = id)
    if request.method=='POST':
        obj = AddUser(request.POST,instance = uedit)
        if obj.is_valid():
            obj.save()
            return index(request)
    
    obj=AddUser(instance=uedit)
    return render(request,'uedit.html',{'data':obj})

def pedit(request, id):
    pedit = Product.objects.get(id = id)
    if request.method=='POST':
        obj = AddProduct(request.POST,instance = pedit)
        if obj.is_valid():
            obj.save()
            return index(request)
    
    obj=AddProduct(instance=pedit)
    return render(request,'pedit.html',{'data':obj})

def uedit(request, id):
    uedit = User.objects.get(id = id)
    if request.method=='POST':
        obj = AddUser(request.POST,instance = uedit)
        if obj.is_valid():
            obj.save()
            return index(request)
    
    obj=AddUser(instance=uedit)
    return render(request,'uedit.html',{'data':obj})

def oedit(request, id):
    oedit = Order.objects.get(id = id)
    if request.method=='POST':
        obj = AddOrder(request.POST,instance = oedit)
        if obj.is_valid():
            obj.save()
            return index(request)
    
    obj=AddOrder(instance=oedit)
    return render(request,'oedit.html',{'data':obj})
