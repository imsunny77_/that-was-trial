from django.db import models

# Create your models here.
class User(models.Model):
    user_name = models.CharField(max_length=20)
    user_city = models.CharField(max_length=20)
    def __str__(self):
        return self.user_name

class Product(models.Model):
    product_name = models.CharField(max_length=20)
    price = models.IntegerField()
    def __str__(self):
        return self.product_name

class Order(models.Model):
    user_name = models.ForeignKey(User,on_delete=models.CASCADE)
    product_name = models.ForeignKey(Product,on_delete=models.CASCADE)
    address = models.CharField(max_length=100)

    def __str__(self):
        return self.user_name, self.product_name

