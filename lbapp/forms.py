from django import forms
from . models import *

class AddUser(forms.ModelForm):
    class Meta:
        model = User
        fields = '__all__'

class AddProduct(forms.ModelForm):
    class Meta:
        model = Product
        fields = '__all__'

class AddOrder(forms.ModelForm):
    class Meta:
        model = Order
        fields = '__all__'